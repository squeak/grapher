# Installation

Clone [the grapher repository](https://framagit.org/squeak/grapher) wherever you want on your system. Then run:
```bash
cd /path/to/where-you-put/grapher/
npm install
```
This will install the necessary dependencies in `node_modules`.

Then you can start the app with `npm start` and visit your browser at `localhost:8791`.
