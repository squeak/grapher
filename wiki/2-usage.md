# Usage

To use grapher, you should first create a file named `config/graphs.js` in grapher's directory.
This file will be a configuration of all the graph pages you want to have in grapher.

The config file tells grapher how to interpret your data, and how to display it in grapher.

An example of configuration file with two graph pages:

```js
var _ = require("underscore");
var $$ = require("squeak");

function periodDuration (groups) {
  return _.mapObject(groups, function (groupDays) {
    var period = _.reduce(groupDays, function (memo, dayObj) { return memo + dayObj.duration; }, 0);
    return $$.round(period, 0);
  });
};

module.exports = [

  {
    name: "Accounting",
    data: {
      path: "/home/me/Downloads/Accounting.ods",
      readFile: "xlsx",
      conversion: [ "ods", ],
    },
    charts: [
      {
        name: "year",
        type: "bar",
        dataExtraction: function (collection) {
          return periodDuration(_.groupBy(collection, "year"));
        },
        sortCounts: "label",
      },
      {
        name: "month",
        type: "bar",
        dataExtraction: function (collection) {
          return periodDuration(_.groupBy(collection, "month"));
        },
        sortCounts: "label",
      },
      {
        name: "week",
        type: "bar",
        dataExtraction: function (collection) {
          return periodDuration(_.groupBy(collection, "week"));
        },
        sortCounts: "label",
      },
      {
        name: "date",
        type: "bar",
        dataExtraction: function (collection) {
          var result = {};
          _.each(collection, function (dayObj) {
            result[dayObj.date] = dayObj.duration;
          });
          return result;
        },
        sortCounts: "label",
      },
    ],
  },

  {
    name: "Fun",
    data: {
      path: "/home/me/Downloads/NotSoFun.ods",
      readFile: "xlsx",
      conversion: [
        function (rawData) {
          return // my custom code to process and convert data to a comprehensible format for grapher
        },
      ],
    },
    charts: [
      // ...
    ],
  },

];
```
