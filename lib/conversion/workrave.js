var _ = require("underscore");
var $$ = require("squeak");
var dayjs = require("dayjs");
dayjs.extend(require("dayjs/plugin/objectSupport"));
dayjs.extend(require("dayjs/plugin/duration"))
dayjs.extend(require("dayjs/plugin/weekOfYear"))
dayjs.extend(require("dayjs/plugin/advancedFormat"))

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (data) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var splitData = data.split("\n");
  var filteredData = _.filter(splitData, function (string) { return string.match(/^D /) });
  return _.map(filteredData, function (dayData) {
    var dayDataSplit = dayData.split(" ");

    var start = dayjs({
      years: +dayDataSplit[3] + 1900,
      months: +dayDataSplit[2],
      date: +dayDataSplit[1],
      hours: +dayDataSplit[4],
      minutes: +dayDataSplit[5],
    });
    var end = dayjs({
      years: +dayDataSplit[8] + 1900,
      months: +dayDataSplit[7],
      date: +dayDataSplit[6],
      hours: +dayDataSplit[9],
      minutes: +dayDataSplit[10],
    });

    return {
      start: start,
      end: end,
      duration: dayjs.duration(end.diff(start)).as("hours"),

      year: start.format("YYYY"),
      month: start.format("YYYY-MM"),
      week: start.format("YYYY-ww"),
      date: start.format("YYYY-MM-DD"),
    };

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
