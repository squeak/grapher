
# grapher

Grapher is an app that lets you simply make charts out of any set of data.

For the full documentation, installation instructions... check [grapher documentation page](https://squeak.eauchat.org/grapher/).
