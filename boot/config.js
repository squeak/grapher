let path = require("path");

module.exports = {

  name: "grapher", // this will overwrite the name set in package.json, you can safely omit it if you're fine with that one
  port: 8791,

  logColor: ["#0088FF", "#00FF33", "#CCDD00", "#CC0000"],

  appPath: path.join(__dirname +"/../"), // Important, don't change this unless you move this script.

  // set to true to precalculate all dist files on server start
  production: false,

  // for more debugging logs, enable this
  // debug: true,

  // calculate list of submodules to allow to easily find less style files in submodules <false|"always"|"once">
  resolveSubmodulesPathsRecursivelyForLessImportStatments: "once",

}
