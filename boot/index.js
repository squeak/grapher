// NOTE: ONLY MODIFY THIS FILE IF YOU KNOW WHAT YOU'RE DOING

const fs = require("fs");
const $$ = require("squeak/node");

// START ONLY IF THERE IS A GRAPHS CONFIG
if (fs.existsSync("./config/graphs.js")) {
  let electrodeBooter = require("electrode/boot");
  let config = require("./config.js");
  electrodeBooter(config);
}
else {
  $$.log.error("Please create a `config/graphs.js` file specifying the list of graphs before starting grapher.")
};
