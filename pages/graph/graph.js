var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/ajax");
require("squeak/extension/url");
var $ = require("yquerj");
var uify = require("uify");
var graphify = require("graphify");
var grapherConversions = require("../../lib/conversion");
require("electrode/lib/parentPage");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY CLICKED OPERATIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayClickedOperations (operationsList, keysToDisplay) {

  // alert entries matching this period and type
  var uifyAlert = uify.alert(function ($container) {
    var $table = $container.table({ class: "alert-operations", });

    // headers
    var $thead = $table.thead().tr();
    _.each(keysToDisplay, function (key_or_KeyAndLabel) {
      $thead.td({ text: _.isArray(key_or_KeyAndLabel) ? key_or_KeyAndLabel[1] : key_or_KeyAndLabel, });
    });

    // lines
    var $operations = $table.tbody();
    _.each(operationsList, function (operation) {
      var $operation = $operations.tr();
      _.each(keysToDisplay, function (key_or_KeyAndLabel) {
        $operation.td({ text: operation[_.isArray(key_or_KeyAndLabel) ? key_or_KeyAndLabel[0] : key_or_KeyAndLabel], });
      });
    });

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY CHARTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayCharts (graphObject, graphData) {

  // automatically add chartClick event if asked
  if (graphObject.data.clickedOperationsDisplay) _.each(graphObject.charts, function (chartOptions) {
    if (!chartOptions.chartClick && chartOptions.clickedOperationsGetter) chartOptions.chartClick = function (element, datasets, collection, chart, event) {
      var clickedOperationsList = chartOptions.clickedOperationsGetter(element, datasets, collection, chart, event);
      displayClickedOperations(clickedOperationsList, graphObject.data.clickedOperationsDisplay);
    };
  });

  // create graphs
  graphify({
    $container: $app,
    collection: graphData,
    statsOverview: false,
    graphs: graphObject.charts,
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CONVERT DATA
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function convertData (graphObject, data) {
  var finalData = data;
  _.each(graphObject.data.conversion, function (conversionFunc) {
    if (_.isFunction(conversionFunc)) finalData = conversionFunc(finalData)
    else if (grapherConversions[conversionFunc]) finalData = grapherConversions[conversionFunc](finalData)
    else uify.toast.error("Failed to apply conversion: "+ conversionFunc);
  });
  return finalData;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY GRAPH
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayGraph (graphObject) {

  if (!graphObject.title) graphObject.title = graphObject.id;

  // update page title
  $("head title").text("Grapher: "+ graphObject.title);

  // check if some infos could be missing in graph config
  if (!graphObject.data) return uify.alert.error("Missing data info in this graph's config.")
  else if (!graphObject.data.path) return uify.alert.error("Missing data file path.")
  else if (!graphObject.data.fileReader) return uify.alert.error("Missing data file reader to use.");

  // get data from file
  $.ajax({
    url: "/getData/?dataPath="+ $$.url.urlify(graphObject.data.path) +"&dataReader="+ graphObject.data.fileReader,
    success: function (response) {
      // apply necessary data conversions
      var graphData = convertData(graphObject, response.data);
      // display charts
      displayCharts(graphObject, graphData);
    },
    error: function (error) {
      uify.alert.error(error.responseJSON.message +"<br><br>Failed to fetch "+ graphObject.title +" data! Check server side terminal logs for error details.");
    },
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// get list of graphs
var graphsList = require("../../config/graphs");

// get graph id
var query = $$.url.query.getAsObject();

// error in graph specification
if (!graphsList[query.g]) {

  // graph not specified
  if (!query.g) var errorMessage = "You didn't specify which graph you want to see."
  // invalid graph
  else var errorMessage = "The graph you asked doesn't seem to exist.";

  // display error message
  var $error = $app.div({ class: "error", });
  $error.div({
    class: "error_message",
    text: errorMessage,
  });
  $error.a({
    class: "error_redirect",
    href: "/",
    text: "return to graphs list",
  });

}

// display graph
else {
  graphsList[query.g].id = query.g;
  displayGraph(graphsList[query.g]);
};
