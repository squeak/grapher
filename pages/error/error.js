
var $error = $app.div({
  class: "error",
})

$error.div({
  class: "error_message",
  text: "There's been an error or the page you asked doesn't exist",
});
$error.a({
  class: "error_redirect",
  href: "/",
  text: "return to homepage",
});
