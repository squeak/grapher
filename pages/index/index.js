var keyboardify = require("keyboardify");

// NOTE: it may be better to avoid using ES6 syntax in page files because there may be some issues with files minification in production

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY ELEMENTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

//
//                              MAKE THE GIVEN STRING RAINBOW COLORED

function makeRainbowString (string) {
  return _.map(string.split(""), function (letter) {
    return '<span style="color: '+ $$.random.color(1) +'">'+ letter +'</span>';
  }).join("");
};

//
//                              DISPLAY APP NAME AS TITLE

var package = require("../../package.json");

var $title = $app.div({
  class: "title",
  htmlSanitized: makeRainbowString(package.name),
});

//
//                              DISPLAY LIST OF GRAPHS AVAILABLE

var graphs = require("../../config/graphs");
var $list = $app.div({ class: "pages", });
_.each(graphs, function(graphObject, graphId){

  $list.a({
    class: "page",
    htmlSanitized: graphObject.title || graphId,
  }).click(function (e) {
    if (e.ctrlKey) window.open("/graph?g="+ graphId, '_blank').focus()
    else document.location.href = "/graph?g="+ graphId;
  });

});

//
//                              SPACEBAR TIP

$app.div({
  class: "spacebar_tip",
  text: "hit spacebar to stop/start snowflakes animation",
});

//                              ¬
//

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SNOWFLAKES AND OTHER ANIMATIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

//
//                              CREATE SNOWFLAKES

var $snowflakes;
function createSnowflakes () {
  $snowflakes = $app.div({ class: "snowflakes", });
  for (var i = 0; i < 10; i++) { createSnowflake(); }
};

//
//                              CREATE AND ANIMATE SNOWFLAKES

function createSnowflake () {
  var $snowflake = $snowflakes.div({ class: "snowflake", });
  animateSnowflake($snowflake);
};

function animateSnowflake ($snowflake) {

  // move snowflake
  var newDimension = $$.random.integer(5, 70);
  $snowflake.css({
    height: newDimension +"vh",
    width: newDimension +"vh",
    backgroundColor: $$.random.color(1),
    top: $$.random.integer(0, 100) +"vh",
    left: $$.random.integer(0, 100) +"vw",
  });

  // wait and redo it in a few seconds
  setTimeout(function () { animateSnowflake($snowflake); }, $$.random.integer(1000, 5000));

};

//
//                              ANIMATE THE BACKGROUND COLOR

function animateBackgroundColor () {
  // change backgroud color
  $app.css("background-color", $$.random.color(1));
  // wait and redo it in a few seconds
  setTimeout(animateBackgroundColor, $$.random.integer(4000, 10000));
};

//
//                              START ANIMATIONS

animateBackgroundColor();
createSnowflakes();

//                              ¬
//

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SPACEBAR TO ACTIVATE/DEACTIVATE SNOWFLAKES ANIMATION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var pause = false;
keyboardify.bind("space", function () {
  if (pause) createSnowflakes()
  else $snowflakes.empty();
  $title.toggleClass("readable");
  pause = !pause;
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
